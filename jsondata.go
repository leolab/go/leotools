package leotools

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
)

type JSONData map[string]interface{}

func (j *JSONData) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New("error reading data")
	}
	res := map[string]interface{}{}
	err := json.Unmarshal(bytes, &res)
	*j = JSONData(res)
	return err
}

func (j JSONData) Value() (driver.Value, error) {
	if len(j) == 0 {
		return nil, nil
	}
	return json.Marshal(j)
}

func (j JSONData) String() string {
	v, e := j.Value()
	if e != nil {
		return ""
	}
	return v.(string)
}
