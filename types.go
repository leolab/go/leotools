package leotools

import (
	"errors"
	"fmt"
	"strconv"
)

func GetFloat(i interface{}) (float64, error) {
	if i == nil {
		return 0, nil
	}
	switch v := i.(type) {
	case string:
		return strconv.ParseFloat(v, 64)
	case int, int8, int16, int32, int64:
		return float64(v.(int64)), nil
	case float32, float64:
		return v.(float64), nil
	default:
		return 0, errors.New("cannot convert value: " + fmt.Sprintf("%+v", i) + ", type of " + fmt.Sprintf("%T", i))
	}

}

func GetInt(i interface{}) (int64, error) {
	if i == nil {
		return 0, nil
	}
	switch v := i.(type) {
	case int, int8, int16, int32, int64, uint, uint16, uint32, uint64, uint8:
		return v.(int64), nil
	case float32, float64:
		return int64(v.(float64)), nil
	case string:
		if i.(string) == "" {
			return 0, nil
		}
		ret, err := strconv.Atoi(v)
		if err != nil {
			return 0, err
		}
		return int64(ret), nil
	default:
		return 0, errors.New("cannot convert value: " + fmt.Sprintf("%+v", i) + ", type of " + fmt.Sprintf("%T", i))
	}
}

func GetString(i interface{}) string {
	if i == nil {
		return ""
	}
	switch v := i.(type) {
	case string:
		return v
	case []byte:
		return string(v)
	case int, int8, int16, int32, int64:
		return strconv.Itoa(v.(int))
	case float32, float64:
		return strconv.FormatFloat(v.(float64), 'f', 4, 64)
	default:
		return fmt.Sprintf("%x", v)
	}
}

/*
Производит объединение 2-х мап:
Добавляет ключи из 1 во 2-ю
Если в обоих структурах одиноковые ключи - используются данные из 2-го
*/
func JoinMap(m1, m2 map[string]interface{}) (r map[string]interface{}) {
	r = m2
	for k, v := range m1 {
		if _, ok := r[k]; ok {
			//fmt.Println("Key exists: " + k)
			switch r[k].(type) {
			case map[string]interface{}:
				if _, ok := r[k].(map[string]interface{}); ok {
					//fmt.Println("Call recursive for: " + k)
					r[k] = JoinMap(m1[k].(map[string]interface{}), r[k].(map[string]interface{}))
				}
				//Если в m1 не структура?
			default:
				//
			}
		} else {
			r[k] = v
		}
	}
	return r
}
